FROM kerwintsaiii/dind
LABEL MAINTAINER "Kerwin Tsai"
SHELL ["/bin/bash", "-c"]

# Set the locale
#RUN  echo 'Acquire {http::Pipeline-Depth "0";};' >> /etc/apt/apt.conf
#RUN DEBIAN_FRONTEND="noninteractive"
#RUN apt-get update  && apt-get -y install --no-install-recommends locales && locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
EXPOSE 8080

RUN apt-get update && apt-get install -y curl

# See http://bugs.python.org/issue19846
ENV LANG C.UTF-8

RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    git

RUN python3 -m pip --no-cache-dir install --upgrade \
    "pip<20.3" \
    setuptools

# Some TF tools expect a "python" binary
RUN ln -s $(which python3) /usr/local/bin/python

# Options:
#   tensorflow
#   tensorflow-gpu
#   tf-nightly
#   tf-nightly-gpu
# Set --build-arg TF_PACKAGE_VERSION=1.11.0rc0 to install a specific version.
# Installs the latest version by default.
ARG TF_PACKAGE=tensorflow
ARG TF_PACKAGE_VERSION=
RUN python3 -m pip install --no-cache-dir ${TF_PACKAGE}${TF_PACKAGE_VERSION:+==${TF_PACKAGE_VERSION}}

COPY bashrc /etc/bash.bashrc
RUN chmod a+rwx /etc/bash.bashrc

RUN pip3 install kfp numpy==1.18.5 boto3 botocore
RUN git clone https://gitlab.com/jerry-ai-platform/pipeline-handler.git /pipeline-handler && cd /pipeline-handler && git pull
WORKDIR /pipeline-handler