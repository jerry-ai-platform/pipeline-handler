#!/bin/bash
# verify the # of parameters
if [[ -z $1 ]]; then
	echo "Please give the user name !!!"
	exit
fi

if [[ -z $2 ]]; then
	echo "Please give the model folder"
	exit
fi

if [[ -z $3 ]]; then
	echo "Please give the dockerhub account"
	exit
fi

USER="admin"
PASS="123456"
#HOST="pipeline.kerwin.link:30001"
HOST="docker.ai-platform.org"
# Port (for rest_api_port) 
Port=8501

# create a folder 
user=$1
modelName=$2
# modelName=${modelName//\_/\-}
DockerName=$3

mkdir $user-$modelName

# copy .pb file & deployment & service & ingress & Dockerfile into the folder
cp src/tfserving/Dockerfile src/tfserving/service.yaml src/tfserving/deployment.yaml src/tfserving/ingress.yaml $user-$modelName
mkdir 1
cp -r $modelName/* 1 
mv 1 $modelName
cp -r $modelName $user-$modelName

# modify & build Dockerfile
sed -i "s/sample/$modelName/g" $user-$modelName/Dockerfile
sed -i "s/rest_api_port=.*/rest_api_port=$Port/g" $user-$modelName/Dockerfile

docker login --username=$USER --password=$PASS $HOST
docker build $user-$modelName -t $HOST/$DockerName/$user-$modelName:latest --no-cache

# push
docker push $HOST/$DockerName/$user-$modelName:latest

rm -r $user-$modelName
# call backend
#./serving2.sh $user $modelName $DockerName