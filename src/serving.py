import calendar
import os
import time
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow as tf
import pickle
import argparse
from boto3.session import Session
import subprocess
import shutil

def storage_helper_dow(sourceDir: str, bucket_name: str):
    aws_access_key_id = 'minio'
    aws_secret_access_key = 'miniostorage'
    
    session = Session(aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

    s3 = session.resource('s3', endpoint_url='http://10.121.240.233:9000')

    bucket = s3.Bucket(bucket_name)
    for obj in bucket.objects.filter(Prefix = sourceDir):
        if not os.path.exists(os.path.dirname(obj.key)):
            os.makedirs(os.path.dirname(obj.key))
        bucket.download_file(obj.key, obj.key)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Kubeflow FMNIST training script')
    parser.add_argument('--data_dir', help='path to images and labels.')
    #parser.add_argument('--model_path', help='folder to export model')
    parser.add_argument('--bucket_name', help='buckut_name')
    parser.add_argument('--user', help='user_name')
    parser.add_argument('--serv_name', help='serv_name')
    args = parser.parse_args()
    
    #model_path = str(args.model_path)
    model_path = 'save_model'
    bucket_name = str(args.bucket_name)
    user_name = str(args.user)
    serv_name = str(args.serv_name)

    if os.path.isdir(model_path):
        shutil.rmtree("save_model")
    

    storage_helper_dow(model_path, bucket_name)
    #storage_helper_dow("save_model", "1111-dcoquedxic-0707002929")
        # The export path contains the name and the version of the model
    tf.keras.backend.set_learning_phase(0)  # Ignore dropout at inference
    model = tf.keras.models.load_model(model_path+"/saved_model.h5")

    # Fetch the Keras session and save the model
    # The signature definition is defined by the input and output tensors
    # And stored with the default serving key
    for t in model.outputs:
        print('====================')
        print(t.name)
    
    #shutil.rmtree('sample')
    with tf.compat.v1.keras.backend.get_session() as sess:
        tf.compat.v1.saved_model.simple_save(
            sess,
            serv_name,
            inputs={'image': model.input},
            outputs={t.name: t for t in model.outputs})


    subprocess.call(["./src/build.sh", user_name, serv_name, user_name])

    
    if os.path.isdir(serv_name):
        shutil.rmtree(serv_name)
        
    if os.path.isdir(model_path):
        shutil.rmtree(model_path)