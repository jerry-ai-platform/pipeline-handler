import kfp
import kfp.dsl as dsl
from kfp import compiler
from kfp import components
import os
import sys
import json

EXPERIMENT_NAME = 'Testing'

#sys.path.insert(1, 'pipeline/')
from pipeline import train_pipeline_cnn

#client = kfp.Client(host='http://140.114.91.156:31532')
client = kfp.Client(host='https://pip.ai-platform.org')

#experiment = client.create_experiment()

with open("tmp/config.json", "r") as stream:
    data = json.load(stream)


experiment = client.create_experiment(str(data['user']))
#print(str(data['config']['name']))
# Specify pipeline argument values and launch a pipeline run given the pipeline function definition
#arguments = {'images': 'kerwintsaiii/kubeflow-mnist:v1.0.3-gpu', 'bucket_name': 'mnist', 'epoch': '30', 'batch_size': '128'}
arguments = {'data_image': str(data['preprocessing']['image']), 
                'train_image': str(data['train']['image']),
                'serving_image':str(data['serving']['image']),
                'bucket_name': str(data['run_name']), 
                'dataset': str(data['preprocessing']['dataset']),
                'model': str(data['train']['model']),
                'epoch': str(data['train']['epoch']), 
                'batch_size': str(data['train']['batch-size']),
                'user': str(data['user']),
                'run_name': str(data['run_name'])}
                
#client.create_run_from_pipeline_func(train_pipeline_cnn, arguments=arguments,experiment_name=EXPERIMENT_NAME)

pipeline_func = train_pipeline_cnn
pipeline_filename = pipeline_func.__name__ + '.pipeline.zip'
compiler.Compiler().compile(pipeline_func, pipeline_filename)

run_name = pipeline_func.__name__ + ' run'
run_result = client.run_pipeline(experiment.id, run_name, pipeline_filename, arguments)

url = 'https://pip.ai-platform.org/#/runs/details/' + run_result.id
print(url)

# Log file
with open("tmp/url", 'w') as outfile:
    json.dump(url, outfile)

with open("tmp/id", 'w') as outfile:
    json.dump(run_result.id, outfile)

