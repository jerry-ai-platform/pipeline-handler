import kfp.dsl as dsl
from kfp.dsl import PipelineVolume

# To compile the pipeline:
#   dsl-compile --py pipeline.py --output pipeline.tar.gz
from constants import PROJECT_ROOT, CONDA_PYTHON_CMD, BUCKET_NAME


@dsl.pipeline(
    name='Pipeline',
    description='Jerry AI plaform Training Pipeline job.'
)
def train_pipeline_cnn(train_image: str = '',
                        data_image: str = '',
                        serving_image: str = '',
                        webui_image: str = '',
                        bucket_name: str = '',
                        dataset: str = '',
                        model: str = '',
                        batch_size: str = '',
                        epoch: str = '',
                        user: str ='',
                        run_name: str=''):

    run_bucket_name = str(bucket_name) + '-' + str(BUCKET_NAME)
    
    commands_pre = [f"cd {PROJECT_ROOT}", 
                f"git pull && git checkout gpu",
                f"python3 preprocessing.py --data_dir dataset --bucket_name {run_bucket_name}"]
    
    commands_train = [f"cd {PROJECT_ROOT}", 
                f"git pull && git checkout gpu",
                f"python3 train.py --data_dir dataset --model_path save_model --bucket_name {run_bucket_name} --batch-size {batch_size} --epoch {epoch}"]

    preprocessing = dsl.ContainerOp(
        name='preprocessing',
        image=data_image,
        command=['sh'],
        arguments=[
            '-c', ' && '.join(commands_pre),
        ],
    )

    train = dsl.ContainerOp(
        name='train',
        image=train_image,
        command=['sh'],
        arguments=[
            '-c', ' && '.join(commands_train),
        ],
        file_outputs={'model': PROJECT_ROOT + "/save_model/saved_model.h5"}
    )
    train.set_gpu_limit('1')
    train.after(preprocessing)

    serve_args = [f"python3 /serving/script.py --bucket_name {run_bucket_name} --user {user} --serv_name {run_name}"]

    serve = dsl.ContainerOp(
      name='serve',
      image=serving_image,
      command=['bash'],
      arguments=[
          '-c', ' && '.join(serve_args),
      ],
    )
    serve.after(train)

if __name__ == '__main__':
    import kfp.compiler as compiler
    compiler.Compiler().compile(train_pipeline_cnn, __file__ + '.tar.gz')
