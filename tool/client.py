import http.client, urllib.parse
import json
diag1 = { 
  "name": "train_pipeline",
  "user": "benice",
  "run_name": "mnist",
  "preprocessing": {
    "image": "kerwintsaiii/kubeflow-mnist:v1.0.3-gpu",
    "dataset": "mnist"
  },
  "train": {
    "image": "kerwintsaiii/kubeflow-mnist:v1.0.3-gpu",
    "model": "cnn",
    "batch-size": "128",
    "epoch": "1"
  },
  "serving": {
    "image": "kerwintsaiii/pipeline-serving:v1.0.2",
    "args": []
  },
}

data = json.dumps(diag1)

headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
#conn = http.client.HTTPConnection('api.ai-platdoem.org', 8080)
conn = http.client.HTTPConnection('localhost', 8080)
conn.request('POST', '/pipeline-handler', data.encode('utf-8'), headers)
response = conn.getresponse()

print(response.read().decode('utf-8'))
conn.close()
