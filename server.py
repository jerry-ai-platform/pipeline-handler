from http.server import HTTPServer, BaseHTTPRequestHandler, SimpleHTTPRequestHandler
import json
import os
import re
import requests
from urllib.parse import parse_qs
from datetime import datetime
import shutil

file_name = 'tmp/config.json'
PIPELINE_URL = 'https://pip.ai-platform.org/'

class HTTPStatusError(Exception):
    """Exception wrapping a value from http.server.HTTPStatus"""

    def __init__(self, status, description=None):
        """
        Constructs an error instance from a tuple of
        (code, message, description), see http.server.HTTPStatus
        """
        super(HTTPStatusError, self).__init__()
        self.code = status.code
        self.message = status.message
        self.explain = description

class Request(BaseHTTPRequestHandler):
    def getExecTime(self, start_time, end_time):
        FMT = '%H:%M:%S'
        tdelta = datetime.strptime(str(end_time), FMT) - datetime.strptime(str(start_time), FMT)
        return tdelta
    
    def runDockerBuild(self,query):
        bk = ''.join(e for e in query['bk'] if e != '[' or e != "'" or e != ']')
        user = ''.join(e for e in query['user'] if e != '[' or e != "'" or e != ']')
        servn = ''.join(e for e in query['servn'] if e != '[' or e != "'" or e != ']')
        os.environ['bk']=str(bk)
        os.environ['user']=str(user)
        os.environ['servn']=str(servn)
        print("======== serving stdout===========") 
        os.system('python3 src/serving.py --bucket_name $bk --user $user --serv_name $servn')
        print("==================================")
        
    def end_headers(self):
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'OPTIONS,POST,GET')
        self.send_header('Cache-Control', 'no-store, no-cache, must-revalidate')
        self.send_header('Access-Control-Allow-Headers', '*')
        return super(Request, self).end_headers()

    def do_OPTIONS(self):
        self.send_response(200, "ok")
        self.end_headers()

    def do_POST(self):
        print(self.headers)
        print(self.command)
        req_datas = self.rfile.read(int(self.headers['content-length'])) 
        print("--------------------Recive data----------------")
        res1 = req_datas.decode('utf-8')
        res = json.loads(res1)
        print(res)
        print("--------------------Recive data------------------")

        with open(file_name, 'w') as outfile:
            json.dump(res, outfile)
        
        os.system('python3 src/handler.py')

        url = ''
        run_id = ''
        with open('tmp/url', 'r') as outfile:
            url = outfile.read()
        url = re.sub('"', '', url)
        
        with open('tmp/id', 'r') as outfile:
            run_id = outfile.read()

        data = {
            "url": url,
            "run_id": run_id
        }

        data = json.dumps(data)

        self.send_response(200)
        self.send_header('Content-type','application/json')
        self.end_headers()
        self.wfile.write(data.encode('utf-8'))

    def do_GET(self):
        path, _, query_string = self.path.partition('?')
        query = parse_qs(query_string)
        try:
            if path == "/run_status":
                if "id" in query: 
                    run_id = ''.join(e for e in query['id'] if e != '[' or e != "'" or e != ']')
                    r = requests.get(PIPELINE_URL+'apis/v1beta1/runs/'+run_id)
                    r_data = json.loads(r.text)
                    data = {}
                    if "error" in r_data:
                        data = {
                            "error": "pipeline run not found"
                        }
                    else:
                        data = {
                            "name": r_data['run']['name'],
                            "status": r_data['run']['status'],
                            "created_at": r_data['run']['created_at'],
                            "finished_at": r_data['run']['finished_at']
                        }
                    data = json.dumps(data)
                    self.send_response(200)
                    self.send_header('Content-type','application/json')
                    self.end_headers()
                    self.wfile.write(data.encode('utf-8'))
            elif path == "/log_url":
                if "id" in query:
                    run_id = ''.join(e for e in query['id'] if e != '[' or e != "'" or e != ']')
                    r = requests.get(PIPELINE_URL+'apis/v1beta1/runs/'+run_id)
                    r_data = json.loads(r.text)
                    #print(r_data["pipeline_runtime"]["workflow_manifest"])
                    data = {}
                    if "error" in r_data:
                        data = {
                            "error": "pipeline run not found"
                        }
                    else:
                        manifest = json.loads(r_data["pipeline_runtime"]["workflow_manifest"])
                        nodes = manifest["status"]["nodes"]
                        #print(nodes)
                        for i in nodes:
                            if nodes[i]["type"] == "Pod":
                                url = "http://140.114.91.156:31532/artifacts/minio/mlpipeline/artifacts/pipeline-stl5n/"+ i +"/main.log"
                                start_time = re.search('T(.*)Z',nodes[i]["startedAt"])
                                end_time = re.search('T(.*)Z',nodes[i]["finishedAt"])
                                exec_time = self.getExecTime(start_time.group(1), end_time.group(1))
                                stage_tmp = {
                                    str(nodes[i]["displayName"]): {
                                        "log_url": url,
                                        "resourcesDuration": nodes[i]["resourcesDuration"],
                                        "exec_time": str(exec_time)
                                    }
                                }
                                data.update(stage_tmp)        
            
                    data = json.dumps(data)
                    self.send_response(200)
                    self.send_header('Content-type','application/json')
                    self.end_headers()
                    self.wfile.write(data.encode('utf-8'))
            elif path == "/serving":
                if all(x in query for x in ['bk','user','servn']):
                    self.runDockerBuild(query)
                    data = json.dumps({"ok": "job sucess"})
                    self.send_response(200)
                    self.send_header('Content-type','application/json')
                    self.end_headers()
                    self.wfile.write(data.encode('utf-8'))
                else:
                    self.send_response(400)
            else:
                self.send_response(400)
        except HTTPStatusError as err:
            print("[END]")

if __name__ == '__main__':

    host = ('0.0.0.0', 8080)
    server = HTTPServer(host, Request)
    print("Starting server, listen at: %s:%s" % host)

    #open(file_name, 'w').close()
    #open('url', 'w').close()
    
    server.serve_forever()
